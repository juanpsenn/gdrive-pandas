import gspread
import pandas as pd
from googleapiclient.discovery import build, Resource
from gspread import Client
from oauth2client.service_account import ServiceAccountCredentials

from config import CREDENTIALS, SCOPES, DEFAULT_MIMETYPE


def drive_service(*,
                  credentials: str = CREDENTIALS,
                  scopes: list = SCOPES
                  ) -> Resource:
    credentials = ServiceAccountCredentials.from_json_keyfile_name(
        credentials, scopes=scopes)
    gdrive = build('drive', 'v3', credentials=credentials)
    return gdrive


def sheets_service(*,
                   credentials: str = CREDENTIALS,
                   scopes: list = SCOPES
                   ) -> Client:
    gsheets = gspread.service_account(filename=credentials, scopes=scopes)
    return gsheets


def auth_services(*,
                  credentials: str = CREDENTIALS,
                  scopes: list = SCOPES
                  ) -> (Resource, Client):
    gdrive = drive_service(credentials=credentials, scopes=scopes)
    gsheets = sheets_service(credentials=credentials, scopes=scopes)
    return gdrive, gsheets


def get_file(*,
             drive: Resource = None,
             sheet: Client = None,
             folder_id: str,
             mime_type: str = DEFAULT_MIMETYPE
             ) -> pd.DataFrame or None:
    if drive is None:
        drive = drive_service()
    if sheet is None:
        sheet = sheets_service()
    response = drive.files().list(
        q=f"mimeType='{mime_type}' and '{folder_id}' in parents",
        spaces='drive'
    ).execute()
    files = response.get('files')
    if files and len(files) == 1:
        return worksheet_to_df(
            service=sheet,
            key=files[0]['id']
        )
    return None


def worksheet_to_df(*,
                    service: Client = None,
                    key: str
                    ) -> pd.DataFrame:
    if service is None:
        service = sheets_service()
    records = service.open_by_key(key).sheet1.get_all_records()
    return pd.DataFrame(records)
