CREDENTIALS = 'credentials.json'
DEFAULT_MIMETYPE = 'application/vnd.google-apps.spreadsheet'
SCOPES = ['https://www.googleapis.com/auth/drive',
          'https://www.googleapis.com/auth/spreadsheets']
