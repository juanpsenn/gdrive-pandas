from functions import auth_services, get_file

drive, sheets = auth_services()

folders = ['1JLjdNkn5TxOdA_3SgvJK0hpV1ijz2JBm']

for fid in folders:
    print(get_file(drive=drive,
             sheet=sheets,
             folder_id=fid))
